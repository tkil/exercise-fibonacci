import { fibb } from "../index";

describe("Index", () => {
  it.only("should get six digits of fibonacci", () => {
    // Arrange & Act
    const actual = fibb(6);
    // Assert
    const expected = "0, 1, 1, 2, 3, 5";
    expect(actual).toEqual(expected);
  });
});
